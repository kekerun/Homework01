import os
import sys
import cv2 as cv

#用lena图像在HSV空间的实践说明：
#色调通道： 0是红色   取值范围是0~180  以用来表示360度
#饱和度通道： 越灰数值越小   越鲜艳数值越大
#亮度通道：   数值等于max(B,G,R)

def getPathRoot():
    dirname, filename = os.path.split(os.path.abspath(sys.argv[0]))
    dir_project_data = os.path.abspath(os.path.join(dirname, os.path.pardir))
    return dir_project_data

g_filename = "%s\\%s" % (getPathRoot(),  r'images\lena.jpg')

g_datas = {
    "Org":{"coord":(50 + 440*0, 50), "img":None},
    "Blue":{"coord":(50 + 440*1, 50), "img":None},
    "Green":{"coord":(50 + 440*2, 50), "img":None},
    "Red":{"coord":(50 + 440*3, 50), "img":None},

    "Gray":{"coord":(50 + 440*0, 520), "img":None},
    "Hue":{"coord":(50 + 440*1, 520), "img":None},
    "Saturation":{"coord":(50 + 440*2, 520), "img":None},
    "Value":{"coord":(50 + 440*3, 520), "img":None},
}

#在任意window中点击都会打印全部图像的img[x][y]，用来比较同一坐标下数值
def mouse_event(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        window_name = param["window_name"]
        print("----------------------------------")
        for key in g_datas.keys():
            #print(type(g_datas[key]["img"]))
            #if g_datas[key]["img"]:
            tmp_img = g_datas[key]["img"]
            print(key, "x", x, "y", y, "value", tmp_img[x][y])


def make_img(window_name):
    if window_name not in g_datas:
        return
    c_cfg = g_datas[window_name]
    image_dst = cv.imread(g_filename)

    cv.namedWindow(window_name, 1)

    if "coord" in c_cfg:
        cv.moveWindow(window_name, c_cfg["coord"][0], c_cfg["coord"][1])
    if window_name == "Org":
        pass
    elif window_name == "Gray":
        image_dst = cv.cvtColor(image_dst, cv.COLOR_BGR2GRAY)
    elif window_name == "Blue":
        image_dst = image_dst[:, :, 0]
    elif window_name == "Green":
        image_dst = image_dst[:, :, 1]
    elif window_name == "Red":
        image_dst = image_dst[:, :, 2]
    elif window_name == "Hue":
        hsv = cv.cvtColor(image_dst, cv.COLOR_BGR2HSV)
        image_dst = hsv[:, :, 0]
    elif window_name == "Saturation":
        hsv = cv.cvtColor(image_dst, cv.COLOR_BGR2HSV)
        image_dst = hsv[:, :, 1]
    elif window_name == "Value":
        hsv = cv.cvtColor(image_dst, cv.COLOR_BGR2HSV)
        image_dst = hsv[:, :, 2]

    image_dst = cv.resize(image_dst, (int(image_dst.shape[0] / 1.2), int(image_dst.shape[1] / 1.2)))
    g_datas[window_name]["img"] = image_dst
    cv.imshow(window_name, image_dst)
    cv.setMouseCallback(window_name, mouse_event, {"window_name":window_name})


make_img("Org")
make_img("Gray")
make_img("Blue")
make_img("Green")
make_img("Red")
make_img("Hue")
make_img("Saturation")
make_img("Value")



cv.waitKey()
cv.destroyAllWindows()